class CreateHostnames < ActiveRecord::Migration[5.2]
  def change
    create_table :hostnames do |t|
      t.text :domain

      t.timestamps
    end
    add_index :hostnames, :domain, unique: true
  end
end
