class CreateDnsEntriesHostnames < ActiveRecord::Migration[5.2]
  def change
    create_table :dns_entries_hostnames, id: false do |t|
      t.references :dns_entry, foreign_key: true
      t.references :hostname, foreign_key: true

      t.timestamps
    end
  end
end
