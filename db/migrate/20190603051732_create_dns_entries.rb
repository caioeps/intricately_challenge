class CreateDnsEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :dns_entries do |t|
      t.inet :ip

      t.timestamps
    end
  end
end
