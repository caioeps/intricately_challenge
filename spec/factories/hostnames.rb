FactoryBot.define do
  factory :hostname do
    sequence(:domain) { |i| "foo##{i}.com" }
  end
end
