FactoryBot.define do
  factory :dns_entry do
    ip { '127.0.0.1' }

    trait :with_hostname do
      after :create do |dns_entry|
        dns_entry.hostnames << create(:hostname)
      end
    end
  end
end
