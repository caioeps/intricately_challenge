require 'rails_helper'

RSpec.describe DnsEntriesQuery do
  subject(:run) do
    described_class.new(
      included_hostnames: included_hostnames,
      excluded_hostnames: excluded_hostnames,
    ).run.map(&:ip).map(&:to_s)
  end

  let!(:dns_1) { create(:dns_entry, ip: '1.1.1.1') }
  let!(:dns_2) { create(:dns_entry, ip: '2.2.2.2') }
  let!(:dns_3) { create(:dns_entry, ip: '3.3.3.3') }
  let!(:dns_4) { create(:dns_entry, ip: '4.4.4.4') }
  let!(:dns_5) { create(:dns_entry, ip: '5.5.5.5') }

  let!(:lorem) { create(:hostname, domain: 'lorem.com') }
  let!(:ipsum) { create(:hostname, domain: 'ipsum.com') }
  let!(:dolor) { create(:hostname, domain: 'dolor.com') }
  let!(:amet) { create(:hostname, domain: 'amet.com') }
  let!(:sit) { create(:hostname, domain: 'sit.com') }

  let(:included_hostnames) { nil }
  let(:excluded_hostnames) { nil }

  before do
    dns_1.hostnames = [lorem, ipsum, dolor, amet]
    dns_2.hostnames = [ipsum]
    dns_3.hostnames = [ipsum, dolor, amet]
    dns_4.hostnames = [ipsum, dolor, sit, amet]
    dns_5.hostnames = [dolor, sit]
  end

  it 'returns all dns entries' do
    expect(run).to contain_exactly('1.1.1.1', '2.2.2.2', '3.3.3.3', '4.4.4.4', '5.5.5.5')
  end

  context 'when filtering' do
    let(:included_hostnames) { %w(ipsum.com dolor.com) }
    let(:excluded_hostnames) { %w(sit.com) }

    it 'returns only dns_1 and dns_3' do
      expect(run).to contain_exactly('1.1.1.1', '2.2.2.2', '3.3.3.3')
    end
  end
end
