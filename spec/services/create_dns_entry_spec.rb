require 'rails_helper'

RSpec.describe CreateDnsEntry do
  subject(:run) do
    described_class.new(ip, hostnames).run
  end

  let(:ip) { IPAddr.new '127.0.0.1' }

  let(:hostnames) { %w(foo.com bar.com) }

  it 'creates a dns entry' do
    expect { subject }.to change(DnsEntry, :count).by(1)
  end

  it 'creates hostnames' do
    expect { subject }.to change(Hostname, :count).by(2)
  end

  it 'returns an instance of DnsEntry' do
    is_expected.to be_a DnsEntry
  end

  it 'assigns hostnames to dns_entry' do
    subject

    dns_entry = DnsEntry.last

    expect(dns_entry.hostnames.map(&:domain))
      .to contain_exactly(*hostnames)
  end

  context 'when assigning an already existent hostname' do
    before do
      create(:hostname, domain: hostnames.first)
    end

    it 'creates a dns entry' do
      expect { subject }.to change(DnsEntry, :count).by(1)
    end

    it 'creates only one hostname' do
      expect { subject }.to change(Hostname, :count).by(1)
    end

    it 'assigns hostnames to dns_entry' do
      subject

      dns_entry = DnsEntry.last

      expect(dns_entry.hostnames.map(&:domain))
        .to contain_exactly(*hostnames)
    end
  end
end
