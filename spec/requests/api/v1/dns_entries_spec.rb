require 'rails_helper'

RSpec.describe 'api/v1 DNS Entries endpoints' do
  describe 'GET /api/v1/dns_entries' do
    subject do
      get '/api/v1/dns_entries', params: params
    end

    let(:params) do
      {
        per_page: 2,
        page: 1
      }
    end

    let!(:dns_1) { create(:dns_entry, ip: '1.1.1.1') }
    let!(:dns_2) { create(:dns_entry, ip: '2.2.2.2') }
    let!(:dns_3) { create(:dns_entry, ip: '3.3.3.3') }
    let!(:dns_4) { create(:dns_entry, ip: '4.4.4.4') }
    let!(:dns_5) { create(:dns_entry, ip: '5.5.5.5') }

    let!(:lorem) { create(:hostname, domain: 'lorem.com') }
    let!(:ipsum) { create(:hostname, domain: 'ipsum.com') }
    let!(:dolor) { create(:hostname, domain: 'dolor.com') }
    let!(:amet) { create(:hostname, domain: 'amet.com') }
    let!(:sit) { create(:hostname, domain: 'sit.com') }

    let(:included_hostnames) { nil }
    let(:excluded_hostnames) { nil }

    before do
      dns_1.hostnames = [lorem, ipsum, dolor, amet]
      dns_2.hostnames = [ipsum]
      dns_3.hostnames = [ipsum, dolor, amet]
      dns_4.hostnames = [ipsum, dolor, sit, amet]
      dns_5.hostnames = [dolor, sit]
    end

    it 'renders status 200' do
      subject

      expect(response).to have_http_status 200
    end

    it 'renders dns entries' do
      subject

      expect(response.body).to include_json(
        data: [
          {
            id: dns_1.id.to_s
          },
          {
            id: dns_2.id.to_s
          }
        ],
        meta: {
          total_items: 5,
          items: 2,
          last_page: 3
        }
      )
    end

    context 'on the last page' do
      let(:params) do
        {
          per_page: 2,
          page: 3
        }
      end

      it 'renders dns entries' do
        subject

        expect(response.body).to include_json(
          data: [
            {
              id: dns_5.id.to_s
            }
          ],
          meta: {
            total_items: 5,
            items: 1,
            last_page: 3
          }
        )
      end
    end

    context 'overflow page' do
      let(:params) do
        {
          per_page: 2,
          page: 10
        }
      end

      it 'renders dns entries' do
        subject

        expect(response.body).to include_json(
          data: be_empty,
          meta: {
            total_items: 5,
            items: 0,
            last_page: 3
          }
        )
      end
    end
  end

  describe 'POST /api/v1/dns_entries' do
    subject do
      post '/api/v1/dns_entries', params: params
    end

    let(:params) do
      {
        ip: '127.0.0.1',
        hostnames: 'foo.com,bar.com'
      }
    end

    it 'renders status 201' do
      subject

      expect(response).to have_http_status 201
    end

    it 'returns the dns_entry' do
      subject

      expect(response.body).to include_json(
        data: {
          id: /\d+/,
          attributes: {
            ip: '127.0.0.1'
          }
        }
      )
    end
  end
end
