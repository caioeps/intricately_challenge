require 'rails_helper'

RSpec.describe DnsEntry, type: :model do
  it { is_expected.to have_db_column(:ip).of_type(:inet) }
  it { is_expected.to have_many(:hostnames) }
end
