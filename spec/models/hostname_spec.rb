require 'rails_helper'

RSpec.describe Hostname, type: :model do
  it { is_expected.to have_db_column(:domain).of_type(:text) }
  it { is_expected.to have_db_index(:domain).unique }
end
