# Intricately Api Challenge

This project requires Ruby 2.6.3 and Rails 5.2.

1. Setup Database `bundle exec rails db:setup`
2. Setup Database for test env `bundle exec rails db:setup RAILS_ENV=test`
3. Run tests with `bundle exec rspec`

## Available endpoints

### GET /api/v1/dns_entries

Allowed params are:

* `page`
* `per_page`
* `included_hostnames` - Comma separated list of hostnames to be included.
* `excluded_hostnames` - Comma separated list of hostnames to be excluded.

### POST /api/v1/dns_entries

Allowed params are:

* `ip` - IPv4 of DNS entry
* `hostnames` - Comma separated list of hostnames to be linked

