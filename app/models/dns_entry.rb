class DnsEntry < ApplicationRecord
  has_many :dns_entries_hostnames
  has_many :hostnames, through: :dns_entries_hostnames
end
