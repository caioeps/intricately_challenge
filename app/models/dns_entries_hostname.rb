class DnsEntriesHostname < ApplicationRecord
  belongs_to :dns_entry
  belongs_to :hostname
end
