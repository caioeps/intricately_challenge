module Api::V1
  class DnsEntrySerializer
    include FastJsonapi::ObjectSerializer

    attribute :ip do |dns_entry|
      dns_entry.ip.to_s
    end
  end
end
