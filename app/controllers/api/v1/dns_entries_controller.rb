module Api::V1
  class DnsEntriesController < ::ApplicationController
    include Pagy::Backend

    def index
      dns_entries = DnsEntriesQuery.new(
        included_hostnames: included_hostnames,
        excluded_hostnames: excluded_hostnames
      ).run

      pagination, dns_entries = pagy(dns_entries, items: params[:per_page])

      options = {
        meta: {
          total_items: pagination.count,
          items: pagination.items,
          last_page: pagination.last
        }
      }

      render json: DnsEntrySerializer.new(dns_entries, options).serialized_json
    end

    def create
      create_dns_entry = CreateDnsEntry.new(ip, hostnames)
      dns_entry = create_dns_entry.run
      render json: DnsEntrySerializer.new(dns_entry).serialized_json, status: 201
    end

    private

    def included_hostnames
      (params[:included_hostnames] || '').split(',')
    end

    def excluded_hostnames
      (params[:excluded_hostnames] || '').split(',')
    end

    def ip
      params[:ip]
    end

    def hostnames
      params[:hostnames].split(',')
    end
  end
end
