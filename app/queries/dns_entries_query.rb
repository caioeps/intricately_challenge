class DnsEntriesQuery
  def initialize(included_hostnames: nil, excluded_hostnames: nil)
    @included_hostnames = included_hostnames.presence || []
    @excluded_hostnames = excluded_hostnames.presence || []
  end

  def run
    DnsEntry.all
      .then { |dns_entries| filter_included_hostnames(dns_entries) }
      .then { |dns_entries| filter_excluded_hostnames(dns_entries) }
  end

  private

  def filter_included_hostnames(dns_entries)
    return dns_entries unless @included_hostnames.present?

    dns_entries.where(id: dns_entries.joins(:hostnames).merge(included_hostnames).select(:id))
  end

  def filter_excluded_hostnames(dns_entries)
    return dns_entries unless @excluded_hostnames.present?

    dns_entries.where.not(id: dns_entries.joins(:hostnames).merge(excluded_hostnames).select(:id))
  end

  def included_hostnames
    Hostname.where(domain: @included_hostnames)
  end

  def excluded_hostnames
    Hostname.where(domain: @excluded_hostnames)
  end
end
