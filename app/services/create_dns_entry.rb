class CreateDnsEntry
  attr_reader :ip, :string_hostnames

  # @param [IpAddr] ip
  # @param [Array<String>] hostnames
  def initialize(ip, hostnames)
    @ip        = ip
    @string_hostnames = hostnames
  end

  # @return [DnsEntry] The created dns_entry
  def run
    within_transaction do
      create_dns_entry!
      save_hostnames!
      assign_hostnames_to_dns_entry
    end

    dns_entry
  end

  private

  def within_transaction
    ApplicationRecord.transaction do
      yield
    end
  end

  def create_dns_entry!
    dns_entry.save!
  end

  def save_hostnames!
    string_hostnames.each do |string_hostname|
      attrs = { domain: string_hostname }
      Hostname.find_by(attrs) || Hostname.create!(attrs)
    end
  end

  def assign_hostnames_to_dns_entry
    dns_entry.hostnames = Hostname.where(domain: string_hostnames)
  end

  def dns_entry
    @dns_entry ||= DnsEntry.new(ip: ip)
  end
end
